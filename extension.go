package gwerror

func GetError(err error) (*Error, error) {
	customErr, ok := err.(*Error)
	if !ok {
		return nil, err
	}

	return customErr, nil
}

func IsExistErrorByCode(code string, err error, searchingInDeep bool) (bool, error) {
	customErr, ok := err.(*Error)
	if !ok {
		return false, err
	}

	if searchingInDeep {
		currentErr := customErr
		for currentErr.InnerError != nil {
			if currentErr.Code == code {
				return true, nil
			}
			currentErr = currentErr.GetInnerError()
		}

		if currentErr.Code == code {
			return true, nil
		}

		return false, nil
	}

	return customErr.Code == code, nil
}

func IsExistErrorByName(name string, err error, searchingInDeep bool) (bool, error) {
	customErr, ok := err.(*Error)
	if !ok {
		return false, err
	}

	if searchingInDeep {
		currentErr := customErr
		for currentErr.InnerError != nil {
			if currentErr.Name == name {
				return true, nil
			}
			currentErr = currentErr.GetInnerError()
		}

		if currentErr.Name == name {
			return true, nil
		}

		return false, nil
	}

	return customErr.Name == name, nil
}

func IsExistError(code string, name string, err error, searchingInDeep bool) (bool, error) {
	customErr, ok := err.(*Error)
	if !ok {
		return false, err
	}

	if searchingInDeep {
		currentErr := customErr
		for currentErr.InnerError != nil {
			if currentErr.Name == name && currentErr.Code == code {
				return true, nil
			}
			currentErr = currentErr.GetInnerError()
		}

		if currentErr.Name == name && currentErr.Code == code {
			return true, nil
		}

		return false, nil
	}

	return customErr.Name == name && customErr.Code == code, nil
}
