package gwerror_test

import (
	"errors"
	"fmt"
	"testing"

	"bitbucket.org/lebronto_kerovol/gwerror"
)

func TestIsExistError(t *testing.T) {
	gwerr := gwerror.NewError("1", "one", errors.New("One error!"))
	gwerr = gwerror.WrapError("2", "two", errors.New("Two error!"), gwerr)
	gwerr = gwerror.WrapError("3", "thr", errors.New("Three error!"), gwerr)

	isExist, err := gwerror.IsExistError("3", "thr", gwerr, true)

	t.Log(fmt.Sprintf("IsExist: %v\nError: %+v", isExist, err))
}
