package gwerror_test

import (
	"encoding/json"
	"errors"
	"fmt"
	"testing"

	"bitbucket.org/lebronto_kerovol/gwerror"
)

func TestError(t *testing.T) {
	sysErr := gwerror.NewError(string("1"), "One", errors.New("One error!"))
	sysErr = sysErr.Add(errors.New("Two"), sysErr)
	sysErr = sysErr.Add(errors.New("Three"), sysErr)
	sysErr = sysErr.AddFromString("4", "error-handling", "Four", sysErr)

	resultMap := sysErr.ToMap(true)
	t.Log(fmt.Sprintf("Map: %v", resultMap))

	b, err := json.Marshal(resultMap)
	if err != nil {
		t.Error(err)
	}

	t.Log(fmt.Sprintf("JSON: %s", string(b)))
}
