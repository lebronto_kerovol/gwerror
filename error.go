package gwerror

import (
	"errors"
	"fmt"
)

type Error struct {
	Code        string `json:"code"`
	Name        string `json:"name"`
	Description string `json:"description"`
	InnerError  error  `json:"innerError"`

	ptrError *error `json:"-"`
}

func (e *Error) GetInnerError() *Error {
	switch (*e).InnerError.(type) {
	case *Error:
		err := (*e).InnerError.(*Error)
		return err
	default:
		return nil
	}
}

func (e *Error) GetError() error {
	return *e.ptrError
}

func NewError(code, name string, err error) *Error {
	resultErr := Error{Code: code, Name: name, Description: err.Error(), ptrError: &err, InnerError: nil}
	return &resultErr
}

func NewErrorFromString(code, name, description string) *Error {
	err := errors.New(description)
	result := Error{Code: code, Name: name, Description: description, ptrError: &err, InnerError: nil}
	return &result
}

func WrapError(code, name string, err error, innerError error) *Error {
	resultErr := Error{Code: code, Name: name, Description: err.Error(), ptrError: &err, InnerError: innerError}
	return &resultErr
}

func WrapErrorFromString(code, name, description string, innerError error) *Error {
	err := errors.New(description)
	result := Error{Code: code, Name: name, Description: description, ptrError: &err, InnerError: innerError}
	return &result
}

func (e *Error) Add(newError error, innerError error) *Error {
	tempErr, ok := newError.(*Error)
	if ok {
		err := tempErr
		err.InnerError = e
		return err
	}

	err := NewError("", "", newError)
	err.InnerError = e
	return err
}

func (e *Error) AddFromString(code, name, description string, innerError error) *Error {
	err := NewErrorFromString(code, name, description)
	err.InnerError = e
	return err
}

func (e *Error) ToString() string {
	return fmt.Sprintf("%+v\n", e)
}

const (
	JSON_CODE        = `code`
	JSON_NAME        = `name`
	JSON_DESCRIPTION = `description`
	JSON_INNER_ERROR = `innerError`
)

func (e *Error) ToMap(includInnerError bool) map[string]interface{} {
	if includInnerError {
		return map[string]interface{}{JSON_CODE: e.Code, JSON_NAME: e.Name, JSON_DESCRIPTION: e.Description, JSON_INNER_ERROR: (*e).InnerError}
	} else {
		return map[string]interface{}{JSON_CODE: e.Code, JSON_NAME: e.Name, JSON_DESCRIPTION: e.Description}
	}
}

func getMessageError(err *error) string {
	return (*err).Error()
}

func (e *Error) Error() string {
	messageError := ``

	for e != nil {
		messageError += getMessageError(e.ptrError)
		e = e.GetInnerError()
	}

	return messageError
}
